#!/usr/bin/python3
# An object of Flask class is our WSGI application
from flask import Flask
from datetime import date

# Flask constructor takes the name of current
# module (__name__) as argument
app = Flask(__name__)

# route() function of the Flask class is a
# decorator, tells the application which URL
# should call the associated function
@app.route("/")
def hello_world():
   return "Hello World"


# Json output
@app.route("/date")
def get_current_date():
    return {"Date ": date.today()}


if __name__ == "__main__":
   app.run(host="0.0.0.0", port=2224) # runs the application
   # app.run(host="0.0.0.0", port=2224, debug=True) # DEBUG MODE

