#!usr/bin/python3

import requests


# define base URL
URL = "https://swapi.dev/api/planets/1"

def main():

    # Make HTTP GET request using requests
    # JSON attachment as pythonic data structure
    # Augment the base URL with a limit parameter to 1000 results
    planet = requests.get(f"{URL}?limit=1000")
    planet = planet.json()

    #print(planet["name"])
    print("Planet number one name is " + planet.get("name"))

if __name__ == "__main__":
     main()
